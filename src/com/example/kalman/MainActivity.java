package com.example.kalman;

import java.io.IOException;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.androidplot.xy.*;
import com.example.kalman.Client;


public class MainActivity extends Activity implements SensorEventListener {
	private static final int HISTORY_SIZE = 300;
	private SensorManager sensorMgr = null;
	private Sensor gyroSensor = null;
	private Sensor accSensor = null;
	private Client client = null;
	private WebServer server;

	private double[][] P = { { 1, 0 }, { 0, 1 } };
	private double angle = 10;
	private double bias = 0;

	private final double R_acc = 10;
	private final double Q_angle = 1e-2;
	private final double Q_bias = 1e-5;

	private XYPlot aprHistoryPlot = null;

	private double timestamp;

	private SimpleXYSeries accAngleHistorySeries = null;
	private SimpleXYSeries angleHistorySeries = null;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// setup the APR History plot:
		aprHistoryPlot = (XYPlot) findViewById(R.id.aprHistoryPlot);

		accAngleHistorySeries = new SimpleXYSeries("Acc_angle");
		accAngleHistorySeries.useImplicitXVals();
		angleHistorySeries = new SimpleXYSeries("Angle");
		angleHistorySeries.useImplicitXVals();

		aprHistoryPlot.setRangeBoundaries(-90, 90, BoundaryMode.AUTO);
		aprHistoryPlot.setDomainBoundaries(0, 300, BoundaryMode.FIXED);
		aprHistoryPlot.addSeries(accAngleHistorySeries,
				new LineAndPointFormatter(Color.GREEN, 0, 0, null));
		aprHistoryPlot.addSeries(angleHistorySeries, new LineAndPointFormatter(
				Color.YELLOW, 0, 0, null));
		aprHistoryPlot.setDomainStepValue(5);
		aprHistoryPlot.setTicksPerRangeLabel(3);

		// register for orientation sensor events:
		sensorMgr = (SensorManager) getApplicationContext().getSystemService(
				Context.SENSOR_SERVICE);
		for (Sensor sensor : sensorMgr.getSensorList(Sensor.TYPE_ALL)) {
			if (sensor.getType() == Sensor.TYPE_GYROSCOPE_UNCALIBRATED) {
				gyroSensor = sensor;
			}

			if (sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
				accSensor = sensor;
			}
		}

		// if we can't access the orientation sensor then exit:
		if (gyroSensor == null) {
			System.out.println("Failed to attach to gyro.");
			cleanup();
		}

		if (accSensor == null) {
			System.out.println("Failed to attach to acc.");
			cleanup();
		}

		sensorMgr.registerListener(this, gyroSensor,
				SensorManager.SENSOR_DELAY_FASTEST);
		sensorMgr.registerListener(this, accSensor,
				SensorManager.SENSOR_DELAY_FASTEST);
		
		client = new Client();
		
        final Button button = (Button) findViewById(R.id.button1);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	EditText ip_edit = (EditText) findViewById(R.id.editText1);
            	String ip = ip_edit.getText().toString();
            	
            	EditText port_edit = (EditText) findViewById(R.id.editText2);
            	int port = Integer.valueOf(port_edit.getText().toString());
            	
                client.connect(ip, port);
            }
        });
        
        server = new WebServer();
        try {
            server.start();
        } catch(IOException ioe) {
            Log.w("Httpd", "The server could not start.");
        }
        Log.w("Httpd", "Web server initialized.");

	}
	
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if (server != null)
            server.stop();
    }

	private void cleanup() {
		// unregister with the orientation sensor before exiting:
		sensorMgr.unregisterListener(this);
		finish();
	}

	// Called whenever a new orSensor reading is taken.
	@Override
	public synchronized void onSensorChanged(SensorEvent sensorEvent) {
		if (sensorEvent.sensor.getType() == Sensor.TYPE_GYROSCOPE_UNCALIBRATED) {
			// predict update
			predict(sensorEvent.values[0] * 180.0 / Math.PI, sensorEvent.timestamp);
		}

		if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			// using accelerometer measurements we can obtain noisy angle
			double acc_angle = Math.atan2(sensorEvent.values[1],
					sensorEvent.values[2]) * 180.0 / Math.PI;

			// measurement update
			measure(acc_angle);
		}
	}

	// prediction update
	// X = [angle; bias]; - estimated vector
	// F = [1 -dt; 0 1]; - prediction model
	// B = [dt; 0]; - control input model (unbiased gyroscope)
	public void predict(double measured_gyro, long t) {
		if (timestamp != 0) {
			// calculate dt, using previously saved timestamp
			final double dt = (t - timestamp) * 1e-9;
			
			// X = F * X + B * gyro;
			// angle = angle + measured_gyro * dt - bias * dt;

			// calculate new unbiased gyro using estimated bias value
			final double unbiased_gyro = measured_gyro - bias;

			// predict new angle using unbiased gyro measurement
			angle += unbiased_gyro * dt;

			// update covariance matrix P
			// P = F * P * F' + Q;
			P[0][0] += dt * (dt * P[1][1] - P[0][1] - P[1][0] + Q_angle);
			P[0][1] -= dt * P[1][1];
			P[1][0] -= dt * P[1][1];
			P[1][1] += Q_bias * dt;

			// update plot
			if (angleHistorySeries.size() > HISTORY_SIZE)
				angleHistorySeries.removeFirst();

			angleHistorySeries.addLast(null, angle);
			aprHistoryPlot.redraw();

		}

		timestamp = t;
	}

	// measurement update
	// H = [1 0]; - observation model
	public void measure(double acc_angle) {
		// calculate innovation as diff between measured and estimated angles
		// y = z - H * X;
		double innov = acc_angle - angle;

		// calculate innovation covariance
		// S = H * P * H' + R
		double S = P[0][0] + R_acc;

		// calculate Kalman gain vector
		// K = P * H' / S;
		double K0 = P[0][0] / S;
		double K1 = P[1][0] / S;

		// update estimates
		// X = X + K * innov;
		angle += K0 * innov;
		bias += K1 * innov;

		// update covariance matrix P
		// P = (I - K * H) * P;
		P[0][0] -= K0 * P[0][0];
		P[0][1] -= K0 * P[0][1];
		P[1][0] -= K1 * P[0][0];
		P[1][1] -= K1 * P[0][1];

		// update plot
		if (accAngleHistorySeries.size() > HISTORY_SIZE)
			accAngleHistorySeries.removeFirst();

		accAngleHistorySeries.addLast(null, acc_angle);
		aprHistoryPlot.redraw();

		String str = "Angle: " + String.valueOf(angle) + 
				" Accelerometer angle: " + String.valueOf(acc_angle);
		client.send(str);
		
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int i) {
		// Not interested in this event
	}
	
    private class WebServer extends NanoHTTPD {

        public WebServer()
        {
            super(8080);
        }

        @Override
        public Response serve(String uri, Method method, 
                              Map<String, String> header,
                              Map<String, String> parameters,
                              Map<String, String> files) {
            String str = 
            		"<html>" +
            		"<head>" +
            		"<script type=\"text/JavaScript\">" +
                    "function timedRefresh(timeoutPeriod) {setTimeout(\"location.reload(true);\",timeoutPeriod);}" +
            		"</script>" +
            		"</head>" +
            		"<body onload=\"timedRefresh(1);\">" +
            		"Angle: %f" +
            		"</body>" +
            		"</html>";

            return new NanoHTTPD.Response(String.format(str, angle));
        }
    }
}